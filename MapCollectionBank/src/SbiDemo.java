
public class SbiDemo extends RbiInterface {

	int balance;

	public SbiDemo(int balance) {
		this.balance=balance;
		
	}
	
	public void deposit(int depamt) {
		this.balance = this.balance+depamt;
		System.out.println("DEPOSITED AMOUNT = "+depamt);
	}
	public void withdrawl(int wdamt){
		this.balance = this.balance-wdamt;
		System.out.println("WITHDRAWED AMOUNT = "+wdamt);
	}
	public void RBI(int opening_balance){
		this.balance = opening_balance;
	}
	public void showBal(){
		System.out.println("BALANCE = "+this.balance);
	}
	public String toString() {
		String output ="current balance = " +this.balance + "\n";
		return output;
		
	}

	
	
}
