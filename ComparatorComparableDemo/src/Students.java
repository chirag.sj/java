
public class Students implements Comparable<Students>{
	int age;
	String city;
	String name;
	public Students(int a , String c , String n){
		this.age=a;
		this.city=c;
		this.name=n;
	}

	public String toString() {
		String output = "\n \n name ="+this.name;
		output +="\n age ="+this.age;
		output +="\n city ="+this.city;
		return output;
	}

	//	@Override
	//	public int compareTo(Students ob) {
	//		return ob.age - this.age;
	//	}

	@Override
	public int compareTo(Students ob) {
		return this.city.compareTo(ob.city);
	}
}