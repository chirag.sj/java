import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

class AgeComparator implements Comparator<Students> {

	@Override
	public int compare(Students std1, Students std2) {
		return std2.age - std1.age;
	}
	
}

class NameComparator implements Comparator<Students> {

	@Override
	public int compare(Students std1, Students std2) {
		return std1.name.compareTo(std2.name);
	}
	
}

public class ComparableDemo {
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			
			ArrayList students = new ArrayList();
			
			Students std1 = new Students(18,"mumbai","chirag");
			Students std2 = new Students(19,"nagpur","rahul");
			Students std3 = new Students(19,"ahmdabad","jenil");
			Students std4 = new Students(25, "hello", "Chirag2");
			
			
			students.add(std1);
			students.add(std2);
			students.add(std3);
			students.add(std4);
			
			Collections.sort(students, new NameComparator());
			
			
	//		System.out.println(students);
			Iterator i = students.iterator();
			while(i.hasNext()) {
				System.out.println(i.next());
			}
		}

	}
