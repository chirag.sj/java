class Animal{

	public void eat(){
		System.out.println(" i am eating!!!");
	}
}	
class Dog extends Animal{

	public void barks(){
		System.out.println("DOG BARKS!!!");
		
	}
}	
class Lion extends Dog{

	public void roars(){
		System.out.println("LION ROARS!!!");
	}
}	
class TestInheritance{
	public static void main(String[] args){
		Animal ani1=new Animal();
		ani1.eat();

		Dog doggy1=new Dog();
		doggy1.eat();
		doggy1.barks();

		Lion lio1=new Lion();
		lio1.roars();
	}

}
