package arrays;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListDemo {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		al.add(22);
		al.add("karan");
		al.remove("karan");
		al.add(1222);
		al.add("jenil");
		al.add(3322);
		al.add("shrey");
		al.add(787822);
		al.add("rahul");
		al.add(69);
		al.add("manthan");
		System.out.println(al.size());
		System.out.println(al);
		al.remove(al.indexOf(22));
		System.out.println(al);
		al.remove(al.indexOf(1222));
		System.out.println(al);
		al.remove(al.indexOf(3322));
		System.out.println(al);
		al.remove(al.indexOf(787822));
		System.out.println(al);
		al.remove(al.indexOf(69));
		System.out.println(al);
		Collections.sort(al);
		System.out.println(al);
		ArrayList WD = new ArrayList();
		WD.add("monday");
		WD.add("tuesday");
		WD.add("wednesday");
		WD.add("thursday");
		WD.add("friday");
		System.out.println(WD);
		ArrayList WE = new ArrayList();
		WE.add("saturday");
		WE.add("sunday");
		System.out.println(WE);
		WD.addAll(WE);
		System.out.println(WD);
		ArrayList num = new ArrayList();
		num.add(1);
		num.add(2);
		num.add(3);
		System.out.println(num);
		WD.addAll(num);
		System.out.println(WD);
		
	}
}
