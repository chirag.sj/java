package com.multi;

public class BankDemo {
	int balance;
	
	public BankDemo(int balance) {
		this.balance = balance;
		this.showbal();
	}
	public void deposit(int damt) {
		synchronized(this) {
			this.balance = this.balance+damt;
			System.out.println("amount deposited :"+damt);
			notify();
		
		}
	}
	
	
	public void withdrawl(int wamt) throws InterruptedException {
		synchronized(this) {
			
				this.balance = this.balance - wamt;
				System.out.println("amount withdrawl :"+wamt);
			} 
		}
	
	
	public void showbal() {
		System.out.println("current balance : "+this.balance);
	}	
}

