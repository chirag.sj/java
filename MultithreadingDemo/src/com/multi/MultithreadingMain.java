package com.multi;

public class MultithreadingMain {

	public static void main(String[] args) throws InterruptedException {
		Hi hiobj = new Hi();
		Hello helloobj = new Hello();
		
		hiobj.start();
		helloobj.start();

	}

}
