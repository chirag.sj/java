package com.multi;

public class BankMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			BankDemo acc1 = new BankDemo(4000);
			
			DepositThread dt = new DepositThread(acc1, 2000);
			WithdrawThread wt = new WithdrawThread(acc1, 5000);
	
			dt.start();
			wt.start();
	}

}
