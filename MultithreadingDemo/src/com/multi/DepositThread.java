package com.multi;

public class DepositThread extends Thread{
	
	BankDemo acc;
	int damt;
	
	public DepositThread(BankDemo acc1, int amt) {
		this.acc = acc1;
		this.damt = amt;
	}
	
	public void run() {
		this.acc.deposit(this.damt);
		acc.showbal();
	}
}
