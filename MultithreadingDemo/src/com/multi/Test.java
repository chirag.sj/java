package com.multi;

class Test{  
	public static void main(String args[]){  
	
		
		final Customer c = new Customer();
		
		Thread wt = new Thread(){ 
			public void run(){
				c.withdraw(15000);
			}
		};
		
		Thread dt = new Thread(){  
	
			public void run(){
				c.deposit(10000);
			}  
		};
		
		wt.start();
		dt.start();  
	  
	}
}  
	


