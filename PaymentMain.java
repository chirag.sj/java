class PaymentMain{
	public static void main(String[] args){
		Payment pay1=new Payment();
		pay1.payment();
	
		Payment pay2=new Payment(22,20);
		pay2.payment();

		Payment pay3=new Payment();
		pay3.setData(22,40);
		pay3.payment();
	}

}
