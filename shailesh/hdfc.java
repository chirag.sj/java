import java.util.Scanner;
class HDFC implements RBIInterface {
	int balance;

	public HDFC(int bal){
		this.balance = bal;
	}
	
	public HDFC(){
		this.balance = 1;
	}
	
	public void showBalance() {
		System.out.println("Current Balance: " + this.balance);
	}


	public void withdraw(int wamt) {
		this.balance = this.balance - wamt;
		System.out.println("amount withdrawn:"+wamt);
	}

	public void deposit(int damt) {
		this.balance = this.balance + damt;
		System.out.println("amount deposited :"+damt );
	}
}
class HDFCMain {
	public static void main(String[] args){
		HDFC hdfc1 = new HDFC(1000);
		int choice;

		do {

		System.out.println("select option :- \n 1. deposit amount \n 2. withdraw amount \n 3. showbalance\n 0. exit");
		Scanner sc = new Scanner(System.in);
		choice = sc.nextInt();
		System.out.println("choice you have selected " + choice);

		switch(choice){
			case 1:
				int damt;
				System.out.println("enter deposit amount:- ");
				damt = sc.nextInt();
				hdfc1.deposit(damt);
				break;
	
			case 2:
				int wamt;
				System.out.println("enter the amount you want to withdraw :- ");
				wamt = sc.nextInt();
				hdfc1.withdraw(wamt);
				break;
	
			case 3:
				hdfc1.showBalance();
				break;
	
			case 0:
				System.out.println("exit! ");
				break;

			default:
				System.out.println("invalid choice");

		}
		} while (choice != 0);
	}
}
