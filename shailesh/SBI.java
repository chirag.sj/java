class SBI implements RBIInterface {

	public void withdraw(int wamt) {
		System.out.println("Withdraw definition in SBI class");
	}

	public void deposit(int damt) {
		System.out.println("Deposit definition in SBI class");
	}

	public String toString(){
		return "Testing SBI class";
	}
}

class SBIMain {
	public static void main(String []args) {
		SBI sbi1 = new SBI();
		System.out.println(sbi1);
		sbi1.deposit(100);
		sbi1.withdraw(500);
	}
}
