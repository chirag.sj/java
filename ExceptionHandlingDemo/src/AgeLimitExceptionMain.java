import java.util.Scanner;

public class AgeLimitExceptionMain {

	public static void main(String[] args) throws AgeLimitException {
		// TODO Auto-generated method stub
		int age;
		System.out.println("enter age for license");
		Scanner sc = new Scanner(System.in);
		age=sc.nextInt();
		if(age<18) {
			 throw new AgeLimitException("use is not valid for license");
		}else {
			System.out.println("user is valid for license");
		}
	}

}
