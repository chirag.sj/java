public class AgeLimitException extends Exception {
	
	public AgeLimitException(String errmsg) {
		super(errmsg);
	}
	
}
