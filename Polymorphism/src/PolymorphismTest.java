interface Shape{
	public void draw();
}
class Circle1 implements Shape{

	@Override
	public void draw() {
		// TODO Auto-generated method stub
System.out.println("CIRCLE DRAWN !!!");
	}
}
class rectangle2 implements Shape{	
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("RECTANGLE DRAWN !!!");
	}
}

class cube2 implements Shape{	
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("CUBE DRAWN !!!");
	}
}
public class PolymorphismTest {
	public static void main(String[] args) {

		Shape c = new Circle1();
		c.draw();
	
		Shape c1 = new rectangle2();
		c1.draw();
	
		Shape c2 = new cube2();
		c2.draw();
	}
}

