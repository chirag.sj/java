class StudentMain{
          public static void main(String[] args){
		// create object in main method
		// as it is entry point of java program
		// there is no meaning of class without object








	  	Student std1 = new Student("Chirag", "Mumbai", 20);
		System.out.println(std1);
		System.out.println(std1.hashCode());
	  	Student std2 = new Student("Shailesh", "Nagpur", 29);
		System.out.println(std2);
		System.out.println(std2.hashCode());
		System.out.println(std1.equals(std2));

		String msg1 = "Hello";
		String msg2 = "Hello";
		System.out.println(msg1.equals(msg2));

	}


}
