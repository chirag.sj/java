import java.util.*;

public class ArrayListTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList fruits = new ArrayList();
		fruits.add("Oranges");
		fruits.add("Apple");
		fruits.add("Banana");
		fruits.add("Banana");
		//fruits.add(123); // not allowed
		System.out.println(fruits);
		HashSet fruits2 = new HashSet();
		fruits2.add("Oranges");
		fruits2.add("Apple");
		fruits2.add("Banana");
		fruits2.add("Banana");
		System.out.println(fruits2);
		LinkedHashSet fruits3 = new LinkedHashSet();
		fruits3.add("Oranges");
		fruits3.add("Apple");
		fruits3.add("Banana");
		fruits3.add("Banana");
		System.out.println(fruits3);
		TreeSet fruits4 = new TreeSet();
		fruits4.add("Oranges");
		fruits4.add("Apple");
		fruits4.add("Banana");
		fruits4.add("Banana");
		System.out.println(fruits4);
		HashMap student1 = new HashMap();
		student1.put("name", "chirag");
		student1.put("state", "maharshtra");
		student1.put("city", "mumbai");
		student1.put(null, "987");
		student1.put(null, "987123s");
		System.out.println(student1);
		LinkedHashMap student2 = new LinkedHashMap();
		student2.put("name" , "chirag");
		student2.put("state", "maharshtra");
		student2.put("city", "mumbai");
		student2.put("name", "123456");
		student2.put("name", "3456");
		System.out.println(student2);
		TreeMap student3 = new TreeMap();
		student3.put("name", "chirag");
		student3.put("state", "maharshtra");
		student3.put("city", "mumbai");
		student3.put("name", "123456");
		student3.put("name", null);
		student3.put("name2", null);
		System.out.println(student3);
		Hashtable student4 = new Hashtable();
		student4.put("name", "chirag");
		student4.put("state", "maharshtra");
		student4.put("city", "mumbai");
		student4.put("name", "123456");
		student4.put("name", "");
		System.out.println(student4);
	}

}
