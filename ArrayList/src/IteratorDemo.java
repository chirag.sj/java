import java.util.*;
public class IteratorDemo {
	public static void main(String[] args) {
		ArrayList<String> fruits = new ArrayList<>();
		fruits.add("Oranges");
		fruits.add("apple");
		fruits.add("mango");
		fruits.add("Banana");
		
		Iterator i = fruits.iterator();
		
		while(i.hasNext()) {
			System.out.println(i.next());
		}
		
		fruits.forEach(System.out::println);
		
	}
}
