
public class StaticTaste {
	static String db = "oracle database connection";
	
	public void modifyDb(String dbname) {
		this.db = dbname;
	}
	
	static void fooBar() {
		System.out.println("This doesn't need object");
		System.out.println("Still objects can use me");
	}

}
