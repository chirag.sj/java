<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% 

	class RectangleDemo{
		int length;
		int breadth;
		
		public RectangleDemo(int l , int b){
			this.length = l ; 
			this.breadth = b ;
		}
		public String toString(){
			String output = "\n length:"+this.length;
			       output += "\n breadth:"+ this.breadth;
			       return output;
		}
		public int area(){
			int a = this.length*this.breadth;
			System.out.println(a);
			return a;
		}
	}

	int l = Integer.parseInt(request.getParameter("l"));
	int b = Integer.parseInt(request.getParameter("b"));
	RectangleDemo rectobj = new RectangleDemo(5,8);
	int area = rectobj.area();
%>	
	Area of rectangle = <%=area%>
<br/>

</body>
</html>