class RBI{
	int balance;
	
	public void deposit(int depamt){
		this.balance=this.balance+depamt;
	} 
	public void withdrawl(int wdamt){

		this.balance=this.balance-wdamt;
	}

	public RBI(int opening_balance){
		this.balance = opening_balance;
	}
	public void showBal(){
		System.out.println("balance:"+this.balance);
	}
	public RBI(){
		this.balance=1;
	}
	
}
class BankMain{
	public static void main(String[] args){
		RBI rbi1=new RBI();
		rbi1.showBal();

		RBI rbi2=new RBI(1000);
		rbi2.showBal();
		rbi2.deposit(500);
		rbi2.showBal();
		rbi2.withdrawl(200);
		rbi2.showBal();
		
		SBI sbi1=new SBI(2000);
		sbi1.showBal();
		sbi1.withdrawl(200);
		sbi1.showBal();

	}
}
class SBI extends RBI{
	public SBI(int bal){
		super(bal);
	}
	public void withdrawl(int amount){
		this.balance=this.balance-amount-20;
	}
}

