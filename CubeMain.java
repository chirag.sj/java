class CubeMain{
	public static void main(String[] args){
		Cube cube1=new Cube();
		cube1.volume();
	
		Cube cube2=new Cube(2,3,4);
		cube2.volume();
		
		Cube cube3=new Cube();
		cube3.setDimensions(4,5,6);
		cube3.volume();
	}


}
