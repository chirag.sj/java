import java.util.ArrayList;
import java.util.Collections;

class Student{
	String name;
	int age;
	String city;
	public Student(String name, int age, String city) {
		this.name=name;
		this.age=age;
		this.city=city;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", city=" + city + "]";
	}
}

public class ComparatorLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList students = new ArrayList();

		Student std1 = new Student("chirag",21,"vikhroli");
		Student std2 = new Student("dimple",18,"bhaynadar");
		Student std3 = new Student("ronak",35,"kalyan");

		students.add(std1);
		students.add(std2);
		students.add(std3);

		//sort by age
		Collections.sort(students, (Student s1, Student s2)->{
			return s2.age - s1.age;
		});

		students.forEach(System.out::println);

		//sort by name
		Collections.sort(students, (Student s1, Student s2)->{
			return s1.name.compareTo(s2.name);
			
		});
		students.forEach(System.out::println);
		
		Collections.sort(students, (Student s1, Student s2)->{
			return s2.city.compareTo(s1.city);
			
		});
		students.forEach(System.out::println);
		
	}

}
