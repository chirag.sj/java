interface MyCalc{
	public void operate(int x, int y);
}
public class MathCalc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyCalc addobj = (int x, int y)->{
			//			System.out.println(x+y);
			System.out.println(x + "+" + y + "=" + (x+y));
		};

		addobj.operate(5, 8);
		addobj.operate(5, 98);
		addobj.operate(50, 8);

		MyCalc subobj = (int x, int y)->{
			//	System.out.println(x-y);
			System.out.println(x + "-" + y + "=" + (x-y));
		};
		subobj.operate(15, 8);
		subobj.operate(115, 98);
		subobj.operate(50, 8);

		MyCalc multobj = (int x, int y)->{
			System.out.println(x + "�" +y + "=" + (x*y));
		};
		multobj.operate(15, 8);
		multobj.operate(115, 98);
		multobj.operate(50, 8);

		MyCalc divobj = (int x, int y)->{
			System.out.println(x + "�" + y + "=" + (x/y));
		};
		divobj.operate(15, 8);
		divobj.operate(115, 98);
		divobj.operate(50, 8);
	}

}
