class Student{

	//declare data members here
	int age;
	String city;
	String name;

	//define default constructor of class
public Student(){
		System.out.println("I am in Student class");
	}

	//define parameterized constructor of class
	//assign local values to datamembers of class
	public Student(String n, String c, int a){

		System.out.println("I am in Student class");
		this.name = n;
		this.city = c;
		this.age  = a;
	}

	//override toString() method of Object class to print object
	public String toString() {
		String output = "\nName: " + this.name;
		output += "\nAge: " + this.age;
		output += "\nCity: " + this.city;
		return "\n--------------\n" + output;
	}

}	
