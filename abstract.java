abstract class RBI{
	int balance;
	
	
	public abstract void deposit(int damt);
	public abstract void withdrawl(int wamt);

	public void showBal(){
		System.out.println("balance = " + this.balance);
	}

	public RBI(){
		this.balance = 1;
	}

	public RBI(int balance){
		this.balance = balance;
	}
}

class SBI extends RBI{

	public SBI(int ob) {
		super(ob);
	}

	public void deposit(float damt){
		this.balance = this.balance + damt;
		System.out.println("amount deposit :" +damt);
	}

	public void withdrawl(int wamt){
		this.balance = this.balance - wamt;
		System.out.println("amount withdrawn :" +wamt);
	}
}
class SBIMain{
	public static void main(String[] args){
	
		SBI sbi1 = new SBI(500);
		sbi1.deposit(500);
		sbi1.showBal();
		sbi1.withdrawl(300);
		sbi1.showBal();
	}
}	
	
