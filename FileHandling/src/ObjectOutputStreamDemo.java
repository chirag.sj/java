import java.io.*;

public class ObjectOutputStreamDemo {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String s = "Hello world!";
		int i = 897648764;

		FileOutputStream fout = new FileOutputStream("test.txt");
		ObjectOutputStream oout = new ObjectOutputStream(fout);

		oout.writeObject(s);
		oout.writeObject(i);

		oout.close();
	
	}
	
}